<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Acerca del proyecto
consiste en un sistema que permite acortar links como un cortador comun, el usuario se loguea y puede tener todos los links que acorto y copiarlos

## instalacion
para realizar la instalacion basta con bajar el sistema y ejecutal
`composer install`
`npm install`

una vez se tenga se debe hacer el migrate en el sistema
`$ php migrate`

si todo sale bien deberia funciona, si no funciona se debe configurar el archivo .env de la base de datos y para eso hay uno de ejemplo que se puede copiar para crearlo

## Instalacion con docker
para realizar la accion debe poseer docker engine instalado, y tambien poseer docker-composer
los comandos siguientes de deben usar
```
$ docker-compose up
```

correr en segundo plano
```
$ docker-compose up -d
```


si corres el primero comando debes ejecutar el siguiente script en otra consola, o en la misma sin cancelar los servicio
### Linux
```
$ sh install.sh
```
### Window
```
$ install.bat
```

cualquier otra cosa puede notificarlo con un issut

