@echo off
docker-compose run artisan key:generate
docker-compose run npm install
docker-compose run artisan migrate
docker-compose run npm run watch